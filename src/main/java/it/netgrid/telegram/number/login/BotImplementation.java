package it.netgrid.telegram.number.login;

import java.io.IOException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import me.shib.java.lib.jtelebot.models.types.ChatId;
import me.shib.java.lib.jtelebot.models.types.KeyboardButton;
import me.shib.java.lib.jtelebot.models.types.ParseMode;
import me.shib.java.lib.jtelebot.models.types.ReplyKeyboardMarkup;
import me.shib.java.lib.jtelebot.models.types.ReplyMarkup;
import me.shib.java.lib.jtelebot.models.updates.Message;
import me.shib.java.lib.jtelebot.models.updates.Update;
import me.shib.java.lib.jtelebot.service.TelegramBot;

@Singleton
public class BotImplementation {
	private Message message;
	private final TelegramBot bot;
	private final TextRecog rec;
	private final Cache cache;
	private Context userContext;
	
	
	@Inject
	public BotImplementation(TelegramBot bot, TextRecog rec, Cache cache){
		this.bot=bot;
		this.rec=rec;
		this.cache = cache;
	}

	public void run(){
		Update[] updates;
		
		try {
			while ((updates = bot.getUpdates()) != null) {
				for (Update update : updates) {
					message = update.getMessage();
					userContext = cache.getContext(message.getChat().getId());
					rec.execute(userContext, message);
					
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
			
	// metodo utility per inviare un messaggio all'utente
	public void send(String response) throws IOException {
		bot.sendMessage(new ChatId(message.getChat().getId()), response);
	}
	
	public void send(String message, ReplyMarkup rm) throws IOException {
		bot.sendMessage(new ChatId(this.message.getChat().getId()), message, ParseMode.Markdown, false, this.message.getMessage_id(), rm);
	}
	
	public void sendContactRequest() throws IOException {
		KeyboardButton kb = new KeyboardButton("Inviami il tuo numero");
		kb.requestContact();

		KeyboardButton[][] kbm = new KeyboardButton[1][1];
		kbm[0][0] = kb;

		ReplyMarkup rm = new ReplyKeyboardMarkup(kbm, false, true);

		this.send("Inviami il tuo numero", rm);
	}


}
