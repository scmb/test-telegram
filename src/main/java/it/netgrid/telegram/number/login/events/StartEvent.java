package it.netgrid.telegram.number.login.events;

import me.shib.java.lib.jtelebot.models.updates.Message;

public class StartEvent {
private Message m;
	
	public StartEvent(Message m){
		this.m=m;
	}
	public Message getMessage(){
		return m;
	}
}
