package it.netgrid.telegram.number.login.events;

import me.shib.java.lib.jtelebot.models.updates.Message;

public class UnknownEvent {
private Message m;
	
	public UnknownEvent(Message m){
		this.m=m;
	}
	public Message getMessage(){
		return m;
	}
}
