package it.netgrid.telegram.number.login;

import com.google.inject.Inject;

import it.netgrid.telegram.number.login.State;
import it.netgrid.telegram.number.login.states.GuestState;

/**
 * La classe Context si occupa di memorizzare lo stato attuale dell'utente nella
 * macchina a stati del login
 * 
 * @author Alberto Harka
 */
public class Context {
	private State s;
	private String loggedUserFirstName = null;
	private String loggedUserPhone = null;
	private String loggedUserMail = null;
	private String loggedUsername = null;
	private int loggedUserId;

	public int getLoggedUserId() {
		return loggedUserId;
	}

	public void setLoggedUserId(int loggedUserId) {
		this.loggedUserId = loggedUserId;
	}

	@Inject
	public Context(GuestState s) {
		this.s=s;
	}

	public String getLoggedUserFirstName() {
		return loggedUserFirstName;
	}

	public void setLoggedUserFirstName(String loggedUserFirstName) {
		this.loggedUserFirstName = loggedUserFirstName;
	}

	public String getLoggedUserPhone() {
		return loggedUserPhone;
	}

	public void setLoggedUserPhone(String loggedUserPhone) {
		this.loggedUserPhone = loggedUserPhone;
	}

	public String getLoggedUserMail() {
		return loggedUserMail;
	}

	public void setLoggedUserMail(String loggedUserMail) {
		this.loggedUserMail = loggedUserMail;
	}

	public String getLoggedUsername() {
		return loggedUsername;
	}

	public void setLoggedUsername(String loggedUsername) {
		this.loggedUsername = loggedUsername;
	}

	public void setState(State s) {
		this.s = s;
	}

	public State getState() {
		return s;
	}

}
