package it.netgrid.telegram.number.login.states;

import java.io.IOException;

import com.google.inject.Inject;
import com.google.inject.Injector;

import it.netgrid.telegram.number.login.BotImplementation;
import it.netgrid.telegram.number.login.Context;
import it.netgrid.telegram.number.login.WooCommerceUtils;
import it.netgrid.telegram.number.login.State;
import it.netgrid.telegram.number.login.events.GivenNumberEvent;
import it.netgrid.telegram.number.login.events.LoginEvent;
import it.netgrid.telegram.number.login.events.StartEvent;
import it.netgrid.telegram.number.login.events.LogoutEvent;
import it.netgrid.telegram.number.login.events.OrderDetailsEvent;
import it.netgrid.telegram.number.login.events.OrderRequestEvent;
import it.netgrid.telegram.number.login.events.CancelEvent;
import it.netgrid.telegram.number.login.events.UnknownEvent;

public class LogRequestState extends StateTemplate {
	
	private final WooCommerceUtils woo;
	private final Injector injector;
	private final BotImplementation bot;
	
	@Inject
	public LogRequestState(WooCommerceUtils woo, Injector injector, BotImplementation bot){
		this.woo=woo;
		this.injector=injector;
		this.bot=bot;
	}
	
	@Override
	public State on(StartEvent e, Context c) {
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(LoginEvent e, Context c) {
		try {
			bot.send("Hai già chiesto il login");
			bot.sendContactRequest();
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(GivenNumberEvent e, Context c) {
		
		if(woo.tryLog(c, e)){
			return injector.getInstance(LoggedState.class);
		}
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(LogoutEvent e, Context c) {
		try {
			bot.send("Almeno Logga prima!");
		} catch (IOException e1) {
			//NOP
		}
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(CancelEvent e, Context c) {
		try {
			bot.send("Login Annullato!");
		} catch (IOException e1) {
			//NOP
		}
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(UnknownEvent e, Context c) {
		try {
			bot.send("Per accedere devi premere su 'Inviami il tuo numero'!");
		} catch (IOException e1) {
			//NOP
		}
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(OrderRequestEvent e, Context c) {
		try {
			bot.send("Devi loggare prima!");
		} catch (IOException e1) {
			//NOP
		}
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(OrderDetailsEvent e, Context c) {
		try {
			bot.send("Devi loggare prima!");
		} catch (IOException e1) {
			//NOP
		}
		return injector.getInstance(GuestState.class);
	}

}
