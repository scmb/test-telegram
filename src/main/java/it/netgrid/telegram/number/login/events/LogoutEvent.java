package it.netgrid.telegram.number.login.events;

import me.shib.java.lib.jtelebot.models.updates.Message;

public class LogoutEvent {
private Message m;
	
	public LogoutEvent(Message m){
		this.m=m;
	}
	public Message getMessage(){
		return m;
	}
}
