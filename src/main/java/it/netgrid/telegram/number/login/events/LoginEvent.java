package it.netgrid.telegram.number.login.events;

import me.shib.java.lib.jtelebot.models.updates.Message;

public class LoginEvent {
private Message m;
	
	public LoginEvent(Message m){
		this.m=m;
	}
	public Message getMessage(){
		return m;
	}
}
