package it.netgrid.telegram.number.login.events;

import me.shib.java.lib.jtelebot.models.updates.Message;

public class OrderRequestEvent {
private Message m;
	
	public OrderRequestEvent(Message m){
		this.m=m;
	}
	public Message getMessage(){
		return m;
	}
}
