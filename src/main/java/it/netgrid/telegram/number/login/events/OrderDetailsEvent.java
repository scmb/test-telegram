package it.netgrid.telegram.number.login.events;

import me.shib.java.lib.jtelebot.models.updates.Message;

public class OrderDetailsEvent {
private Message m;
	
	public OrderDetailsEvent(Message m){
		this.m=m;
	}
	public Message getMessage(){
		return m;
	}
}