package it.netgrid.telegram.number.login.states;

import it.netgrid.telegram.number.login.State;

/**
 * Superclasse per il Template Pattern dell'implementazione degli stati
 * 
 * @author Alberto Harka
 */
abstract class StateTemplate implements State {
	
}