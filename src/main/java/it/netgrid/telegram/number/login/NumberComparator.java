package it.netgrid.telegram.number.login;

import java.util.Comparator;


public class NumberComparator implements Comparator<String> {

	
	public NumberComparator(){
	}
	
	@Override
	public int compare(String o1, String o2) {
		o2.replace(" ", "");
		o2.replace("(", "");
		o2.replace(")", "");
		if (o1.contains(o2)) {
			return 1;
		} else if (o2.contains(o1)) {
			return 1;
		}
		return 0;
	}

}
