package it.netgrid.telegram.number.login;

import java.util.HashMap;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;

import it.netgrid.telegram.number.login.Context;

/**
 * La classe cache si occupa di memorizzare il puntatore al Context che viene
 * inserito dal Main quando viene aperta la chat
 * 
 * @author Alberto Harka
 */

@Singleton
public class Cache {
	HashMap<Long, Context> cache = new HashMap<Long, Context>();
	private final Injector injector;
	
	@Inject
	public Cache(Injector injector) {
		this.injector=injector;
	}

	public Context getContext(Long id) {
		if (!(cache.containsKey(id))) {
			cacheUser(id);
		}
		return cache.get(id);
	}

	public void cacheUser(Long id) {
		cache.put(id, injector.getInstance(Context.class));
	}

}