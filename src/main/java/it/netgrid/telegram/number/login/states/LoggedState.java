package it.netgrid.telegram.number.login.states;

import java.io.IOException;

import com.google.inject.Inject;
import com.google.inject.Injector;

import it.netgrid.telegram.number.login.BotImplementation;
import it.netgrid.telegram.number.login.Context;
import it.netgrid.telegram.number.login.State;
import it.netgrid.telegram.number.login.WooCommerceUtils;
import it.netgrid.telegram.number.login.events.GivenNumberEvent;
import it.netgrid.telegram.number.login.events.LoginEvent;
import it.netgrid.telegram.number.login.events.StartEvent;
import it.netgrid.telegram.number.login.events.LogoutEvent;
import it.netgrid.telegram.number.login.events.OrderDetailsEvent;
import it.netgrid.telegram.number.login.events.OrderRequestEvent;
import it.netgrid.telegram.number.login.events.CancelEvent;
import it.netgrid.telegram.number.login.events.UnknownEvent;

public class LoggedState extends StateTemplate {
	private final WooCommerceUtils woo;
	private final Injector injector;
	private final BotImplementation bot;
	
	@Inject
	public LoggedState(WooCommerceUtils woo, Injector injector, BotImplementation bot){
		this.woo=woo;
		this.injector=injector;
		this.bot=bot;
	}

	@Override
	public State on(StartEvent e, Context c) {
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(LoginEvent e, Context c) {
		try {
			bot.send("Sei già loggato!");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(GivenNumberEvent e, Context c) {
		try {
			bot.send("Sei già loggato, non mi serve il tuo numero.");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(LogoutEvent e, Context c) {
		
		c.setLoggedUserFirstName(null);
		c.setLoggedUserPhone(null);
		c.setLoggedUserMail(null);
		c.setLoggedUsername(null);
		try {
			bot.send("Logout eseguito!");
		} catch (IOException e1) {
			//NOP
		}
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(CancelEvent e, Context c) {
		try {
			bot.send("Nulla da cancellare!");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(UnknownEvent e, Context c) {
		try {
			bot.send("Non so che fare, " + c.getLoggedUserFirstName() + "!");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(OrderRequestEvent e, Context c) {
		woo.getOrders(c, e);
		return this;
	}

	@Override
	public State on(OrderDetailsEvent e, Context c) {
		String req = e.getMessage().getText().replace("/dettagli", "");
		req = req.replaceAll(" ", "");
		if (req.equals("")) {
			try {
				bot.send("Non hai inserito il numero dell'ordine!");
			} catch (IOException e1) {
				//NOP
			}
		} else {
			woo.getOrderDetails(c, e, Integer.valueOf(req));
		}
		return this;
	}

}
