package it.netgrid.telegram.number.login.events;

import me.shib.java.lib.jtelebot.models.updates.Message;

public class CancelEvent {

	private Message m;
	
	public CancelEvent(Message m){
		this.m=m;
	}
	
	public Message getMessage(){
		return m;
	}
	
}
