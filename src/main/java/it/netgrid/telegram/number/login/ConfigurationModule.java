package it.netgrid.telegram.number.login;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;

import it.netgrid.woocommerce.BulkService;
import it.netgrid.woocommerce.CrudService;
import it.netgrid.woocommerce.jersey.BulkServiceManager;
import it.netgrid.woocommerce.jersey.CrudServiceManager;
import it.netgrid.woocommerce.model.Customer;
import it.netgrid.woocommerce.model.Order;
import me.shib.java.lib.jtelebot.service.TelegramBot;

public class ConfigurationModule extends AbstractModule{

	@Override
	protected void configure() {}
	
	@Singleton
	@Provides
	public TelegramBot buildTelegramBot(Configuration config) {
		return TelegramBot.getInstance(config.getTelegramBotAPIKey());
	}
	
	@Singleton
	@Provides
	public Configuration buildConfiguration() {
		return new it.netgrid.telegram.number.login.Configuration() {
			@Override
			public String getTargetUrl() {
				return "https://staging.italpet.com/";
			}

			@Override
			public String getConsumerSecret() {
				return "cs_5afee72e9d74079541343b89a98d592922de0364";
			}

			@Override
			public String getConsumerKey() {
				return "ck_2ac9333cf79b471f7e9933c8da39be10328cb555";
			}

			@Override
			public String getBasePath() {
				return "";
			}

			@Override
			public String getTelegramBotAPIKey() {
				return "187244105:AAEZkwcJrey2uIoIj454sLf0LHYwgPNQtc0";
			}
		};
	}

	@Singleton
	@Provides
	public BulkService<Customer, Integer, Object> buildCustomerBulkService(Configuration config) {
		return BulkServiceManager.createCustomerBulkService(config);
	}

	@Singleton
	@Provides
	public BulkService<Order, Integer, Object> buildOrderBulkService(Configuration config) {
		return BulkServiceManager.createOrderBulkService(config);
	}
	
	@Singleton
	@Provides
	public CrudService<Order, Integer, Object> buildOrderCrudService(Configuration config) {
		return CrudServiceManager.createOrderService(config);
	}
}
