package it.netgrid.telegram.number.login;


import com.google.inject.Inject;

import it.netgrid.telegram.number.login.Context;
import it.netgrid.telegram.number.login.events.CancelEvent;
import it.netgrid.telegram.number.login.events.GivenNumberEvent;
import it.netgrid.telegram.number.login.events.LoginEvent;
import it.netgrid.telegram.number.login.events.LogoutEvent;
import it.netgrid.telegram.number.login.events.OrderDetailsEvent;
import it.netgrid.telegram.number.login.events.OrderRequestEvent;
import it.netgrid.telegram.number.login.events.StartEvent;
import it.netgrid.telegram.number.login.events.UnknownEvent;


import me.shib.java.lib.jtelebot.models.updates.Message;

public class TextRecog {
	Message userMessage;
	Context userContext;
	
	@Inject
	public TextRecog() {
	}

	/**
	 * implementare la gestione della registrazione
	 * 
	 * @param context
	 * @param message
	 */
	public void execute(Context context, Message message) {
		setContext(context);
		setMessage(message);
		String text = message.getText();
		boolean textNotNull = text != null;
		boolean isPhoneNumber = !textNotNull && (message.getContact().getPhone_number() != null);
		if (textNotNull && text.equals("/start")) {
			context.setState(context.getState().on(onStart(message), context));
		} else if (textNotNull && text.equals("/login")) {
			context.setState(context.getState().on(isLogin(message), context));
		} else if (textNotNull && text.equals("/cancel")) {
			context.setState(context.getState().on(isCanc(message), context));
		} else if (textNotNull && text.equals("/logout")) {
			context.setState(context.getState().on(isLogout(message), context));
		} else if (isPhoneNumber) {
			context.setState(context.getState().on(isCred(message), context));
		} else if (textNotNull && text.equals("/ordini")) {
			context.setState(context.getState().on(isOrdReq(message), context));
		} else if (textNotNull && text.contains("/dettagli")) {
			context.setState(context.getState().on(isDetails(message), context));
		} else {
			context.setState(context.getState().on(isUnknown(message), context));
		}

	}

	public StartEvent onStart(Message m) {
		return new StartEvent(m);
	}

	public CancelEvent isCanc(Message m) {
		return new CancelEvent(m);
	}

	public LoginEvent isLogin(Message m) {
		return new LoginEvent(m);
	}

	public GivenNumberEvent isCred(Message m) {
		return new GivenNumberEvent(m);
	}

	public LogoutEvent isLogout(Message m) {
		return new LogoutEvent(m);
	}

	public OrderRequestEvent isOrdReq(Message m) {
		return new OrderRequestEvent(m);
	}

	public UnknownEvent isUnknown(Message m) {
		return new UnknownEvent(m);
	}

	public OrderDetailsEvent isDetails(Message m) {
		return new OrderDetailsEvent(m);
	}

	private void setContext(Context c) {
		userContext = c;
	}

	private void setMessage(Message m) {
		userMessage = m;
	}

}
