package it.netgrid.telegram.number.login;

import java.io.IOException;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.NotFoundException;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import it.netgrid.telegram.number.login.events.GivenNumberEvent;
import it.netgrid.telegram.number.login.events.OrderDetailsEvent;
import it.netgrid.telegram.number.login.events.OrderRequestEvent;
import it.netgrid.woocommerce.BulkService;
import it.netgrid.woocommerce.Configuration;
import it.netgrid.woocommerce.CrudService;
import it.netgrid.woocommerce.model.Customer;
import it.netgrid.woocommerce.model.LineItem;
import it.netgrid.woocommerce.model.Order;

@Singleton
public class WooCommerceUtils {

	private final BotImplementation bot;
	private final BulkService<Customer, Integer, Object> customerBulkService;
	private final BulkService<Order, Integer, Object> orderBulkService;
	private final CrudService<Order, Integer, Object> orderCrudService;
	

	/**
	 * La classe logger gestisce il login al sito di italpet
	 * 
	 */
	@Inject
	public WooCommerceUtils(Configuration config, 
			BotImplementation bot,
			BulkService<Customer, Integer, Object> customerBulkService,
			BulkService<Order, Integer, Object> orderBulkService,
			CrudService<Order, Integer, Object> orderCrudService
			) {
		this.customerBulkService = customerBulkService;
		this.orderBulkService = orderBulkService;
		this.orderCrudService = orderCrudService;
		this.bot=bot;
	}

	// METODI PER IL
	// LOGIN-----------------------------------------------------------------------------------------------|

	/**
	 * Il metodo tryLog controlla se il numero di telefono dell'utente è
	 * presente tra i customer di italpet
	 * 
	 * @param c
	 *            il contesto preso dalla cache
	 * @param e
	 *            l'evento generato dal TextRecog che contiene il messaggio
	 */
	public boolean tryLog(Context c, GivenNumberEvent e) {

		// preparazione richiesta curl
		Map<String, String> map = new HashMap<String, String>();
		map.put("role", "customer");
		map.put("context", "edit");
		map.put("per_page", "100");
		map.put("page", "1");

		// dimensione pagine
		int listSize = 100;

		// ciclo per pagina
		while (!(listSize < 100)) {
			List<Customer> customers = customerBulkService.read(null, null, map);
			listSize = customers.size();
			String contactNumber = e.getMessage().getContact().getPhone_number();
			String customerNumber;
			Comparator<String> comp = new NumberComparator();

			// ciclo per customer
			for (Customer customer : customers) {
				customerNumber = customer.getBillingAddress().getPhone();
				if (comp.compare(contactNumber, customerNumber) == 1) {
					try {
						bot.send("Benvenuto, "+customer.getFirstName()+"!\n\n/ordini per vedere i tuoi ordini in sospeso\n/dettagli seguito dal numero dell'ordine per vedere i dettagli di un ordine specifico");
					} catch (IOException e1) {
						//NOP
					}
					cacheUser(c, customer);
					return true;
				}
			}
		}
		// se lo stato è rimasto lo stesso non è stato eseguito il login
		try {
			bot.send("Questo numero non è registarto!");
		} catch (IOException e1) {
			//NOP
		}
		return false;

	}

	/**
	 * Metodo che registra il login nel contesto
	 * 
	 * @param customer
	 *            l'oggetto Customer preso dal sito di italpet
	 */
	public void cacheUser(Context context, Customer customer) {
		context.setLoggedUserFirstName(customer.getFirstName());
		context.setLoggedUserPhone(customer.getBillingAddress().getPhone());
		context.setLoggedUserMail(customer.getEmail());
		context.setLoggedUsername(customer.getUsername());
		context.setLoggedUserId(customer.getId());
	}

	// METODI PER LA RICHIESTA DEGLI
	// ORDINI----------------------------------------------------------------------------------------|

	/**
	 * Metodo che manda all'utente la lista dei suoi ordini
	 * 
	 * @param c
	 *            il contesto
	 * @param e
	 *            l'evento
	 */
	public void getOrders(Context c, OrderRequestEvent e) {
		//BulkService<Order, Integer, Object> service = BulkServiceManager.createOrderBulkService(config);
		Map<String, String> map = new HashMap<String, String>();

		// se size è 0 alla fine del percorso significa che non ci sono prodotti
		int size;

		// prende gli ordini on-hold
		map.put("status", "on-hold");
		map.put("context", "edit");
		map.put("customer", String.valueOf(c.getLoggedUserId()));
		List<Order> orders = orderBulkService.read(null, null, map);
		size = orders.size();
		// stampa gli ordini on-hold
		viewOrders(orders, 0);

		// prende gli ordini pending
		map.replace("status", "pending");
		orders = orderBulkService.read(null, null, map);
		size += orders.size();
		// stampa gli ordini pending
		viewOrders(orders, 1);

		// prende gli ordini processing
		map.replace("status", "processing");
		orders = orderBulkService.read(null, null, map);
		size += orders.size();
		// stampa gli ordini processing
		viewOrders(orders, 2);

		// prende gli ordini completati
		map.replace("status", "completed");
		orders = orderBulkService.read(null, null, map);
		size += orders.size();
		// stampa gli ordini completati nel giro di una settimana
		viewOrders(orders, 3);

		// se non ha trovato ordini
		if (size == 0) {
			try {
				bot.send("Non ci sono ordini in corso!");
			} catch (IOException e1) {
				//NOP
			}
		}

	}

	/**
	 * Metodo che spedisce all'utente gli ordini in corso
	 * 
	 * @param list
	 *            la lista degli ordini
	 * @param status
	 *            lo stato degli ordini in quella lista
	 */

	public void viewOrders(List<Order> list, int status) {
		for (Order o : list) {
			try{
				switch(status) {
				case 0:
					bot.send("L'ordine " + o.getId() + " è in attesa di essere pagato.");
					break;
				case 1:
					bot.send("L'ordine " + o.getId() + " è in attesa di essere elaborato.");
					break;
				case 2:
					bot.send("L'ordine " + o.getId() + " è in fase di elaborazione.");
					break;
				case 3:
					// completato da meno di una settimana (si da per scontato che
					// una settimana sia il tempo massimo di spedizione)
					if (o.getUpdatedAt().after(new Date(System.currentTimeMillis() - 604800000))) {
						bot.send("L'ordine " + o.getId() + " è stato spedito il giorno " + o.getUpdatedAt());
					}
				default:
				break;	
				}
			} catch (IOException e1) {
				//NOP
			}
		}

	}

	// METODI PER I DETTAGLI DI UN
	// ORDINE--------------------------------------------------------------------------------------|
	/**
	 * Metodo che manda all'utente i dettagli del suo ordine
	 * 
	 * @param c
	 *            il contesto
	 * @param e
	 *            l'evento
	 */
	public void getOrderDetails(Context c, OrderDetailsEvent e, int id) {
		Order order;
		try {
			order = orderCrudService.read(id, null);

		// se la richiesta non da risultato viene generato un
		// NotFoundException
		} catch (NotFoundException ex) {
			try {
				bot.send("Ordine inesistente");
			} catch (IOException e1) {
				//NOP
			}
			
			return;
		}
			
		// controlla i permessi di lettura
		if ( ! this.isContextAuthorizedForOrder(c, order)) {
			try {
				bot.send("Ordine inesistente");
			} catch (IOException e1) {
				//NOP
			}
		} else {
			this.sendOrderDetails(order);
		}

	}
	
	public void sendOrderDetails(Order order) {
		try {
			bot.send("Ordine numero: " + order.getId() + "\nComposto da:");
		} catch (IOException e1) {
			//NOP
		}
		List<LineItem> items = order.getLineItems();
		for (LineItem i : items) {
			try {
				bot.send(i.getName() + "\n" + i.getPrice() + "€" + "\nQuantità:" + i.getQuantity());
			} catch (IOException e1) {
				//NOP
			}
		}
		try {
			bot.send("Costo Totale: " + order.getTotal() + "€");
		} catch (IOException e1) {
			//NOP
		}
	}
	
	public boolean isContextAuthorizedForOrder(Context context, Order order) {
		return order.getCustomerId() == context.getLoggedUserId();
	}

}
