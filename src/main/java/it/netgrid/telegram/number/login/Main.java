package it.netgrid.telegram.number.login;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	public static void main(String[] args) {
		// Inizializzazione del Injector
		Injector injector=Guice.createInjector(new ConfigurationModule());
		BotImplementation bot =injector.getInstance(BotImplementation.class);
		bot.run();
	}

	

}
