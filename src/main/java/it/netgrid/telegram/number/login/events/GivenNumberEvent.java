package it.netgrid.telegram.number.login.events;


import me.shib.java.lib.jtelebot.models.updates.Message;

public class GivenNumberEvent {
private Message m;
	
	public GivenNumberEvent(Message m){
		this.m=m;
	}
	public Message getMessage(){
		return m;
	}
}
