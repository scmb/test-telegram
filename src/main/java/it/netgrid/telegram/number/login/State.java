package it.netgrid.telegram.number.login;

import it.netgrid.telegram.number.login.Context;
import it.netgrid.telegram.number.login.events.CancelEvent;
import it.netgrid.telegram.number.login.events.GivenNumberEvent;
import it.netgrid.telegram.number.login.events.LoginEvent;
import it.netgrid.telegram.number.login.events.LogoutEvent;
import it.netgrid.telegram.number.login.events.OrderDetailsEvent;
import it.netgrid.telegram.number.login.events.OrderRequestEvent;
import it.netgrid.telegram.number.login.events.StartEvent;
import it.netgrid.telegram.number.login.events.UnknownEvent;

/**
 * Interfaccia State del Context Pattern
 * 
 * @author Alberto Harka
 */
public interface State {

	public State on(StartEvent e, Context c);

	public State on(LoginEvent e, Context c);

	public State on(GivenNumberEvent e, Context c);

	public State on(LogoutEvent e, Context c);

	public State on(CancelEvent e, Context c);

	public State on(UnknownEvent e, Context c);

	public State on(OrderRequestEvent e, Context c);

	public State on(OrderDetailsEvent e, Context c);
}
