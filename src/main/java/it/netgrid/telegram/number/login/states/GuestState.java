package it.netgrid.telegram.number.login.states;

import java.io.IOException;

import com.google.inject.Inject;
import com.google.inject.Injector;

import it.netgrid.telegram.number.login.BotImplementation;
import it.netgrid.telegram.number.login.Context;
import it.netgrid.telegram.number.login.State;
import it.netgrid.telegram.number.login.events.GivenNumberEvent;
import it.netgrid.telegram.number.login.events.LoginEvent;
import it.netgrid.telegram.number.login.events.StartEvent;
import it.netgrid.telegram.number.login.events.LogoutEvent;
import it.netgrid.telegram.number.login.events.OrderDetailsEvent;
import it.netgrid.telegram.number.login.events.OrderRequestEvent;
import it.netgrid.telegram.number.login.events.CancelEvent;
import it.netgrid.telegram.number.login.events.UnknownEvent;

public class GuestState extends StateTemplate {

	private final Injector injector;
	private final BotImplementation bot;
	@Inject
	public GuestState(Injector injector, BotImplementation bot){
		this.injector=injector;
		this.bot=bot;
	}
	
	@Override
	public State on(StartEvent e, Context c) {
		try {
			bot.send("Benevuto!");
		} catch (IOException e1) {
			//NOP
		}
		return injector.getInstance(GuestState.class);
	}

	@Override
	public State on(LoginEvent e, Context c) {
		try {
			bot.sendContactRequest();
			return injector.getInstance(LogRequestState.class);
		} catch (IOException e1) {
			e1.printStackTrace();
			return this;
		}

	}

	@Override
	public State on(GivenNumberEvent e, Context c) {
		System.out.println("WRONG WAY");
		return this;
	}

	@Override
	public State on(LogoutEvent e, Context c) {
		try {
			bot.send("Non sei Loggato!");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(CancelEvent e, Context c) {
		try {
			bot.send("Nulla da cancellare");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(UnknownEvent e, Context c) {
		try {
			bot.send("Non so cosa fare");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(OrderRequestEvent e, Context c) {
		try {
			bot.send("Devi essere loggato per richiedere la lista degli ordini in corso");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

	@Override
	public State on(OrderDetailsEvent e, Context c) {
		try {
			bot.send("Devi essere registrato per richiedere i dettagli di un ordine");
		} catch (IOException e1) {
			//NOP
		}
		return this;
	}

}
