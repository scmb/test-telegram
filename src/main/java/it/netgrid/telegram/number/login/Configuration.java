package it.netgrid.telegram.number.login;

public interface Configuration extends it.netgrid.woocommerce.Configuration {

	public String getTelegramBotAPIKey();
}
